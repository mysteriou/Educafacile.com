<footer>
		<div class="container_12">
			<div class="wrapper">
				<div class="grid_8">
					Design Maniacs &copy; 2013 &nbsp;  | &nbsp;  Website Template designed by <a href="http://www.templatemonster.com" rel="nofollow" class="link">TemplateMonster.com</a>

</br>
</br>

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Cette oeuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
			</div>
				<div class="grid_4">
					<div class="social">
						
					</div>
				</div>
			</div>
		</div>
	</footer>
