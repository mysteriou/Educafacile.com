<!DOCTYPE html>

<html lang ="fr">


<head>
<title>
L'heure en anglais
</title>
<?php 

include("section/secure.php");


?>

<meta name = "description" content = "apprendre l'heure en anglais">

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8"/>

<meta name = "author" content = "massa anthony">

<?php 

include("section/piwik.php");

?>

</head>

<body>

<?php 
include("section/header.php");
include("section/nav.php");

?>

<section>
<article class = "center">

<p class ="blancstrong">
<span>
Lundi = Monday <audio controls> <source src = "fichieraudio/monday.ogg"> </audio>
</span>
<br>
<br>
<span>
Mardi = Tuesday <audio controls><source src= "fichieraudio/Tuesday.ogg"></audio>
</span>
<br>
<br>
<span>
Mercedi = Wednesday <audio controls><source src= "fichieraudio/wednesday.ogg"></audio>
</span>
<br>
<br>
<span>
Jeudi = Thursday <audio controls ><source src = "fichieraudio/ukthund009.ogg"></audio>
</span>
<br>
<br>
<span>
Vendredi = Friday <audio controls ><source src = "fichieraudio/friday.ogg"></audio>
</span>
<br>
<br>
<span>
Samedi = Saturday <audio controls> <source src = "fichieraudio/satuday.ogg"></audio>
</span>
<br>
<br>
<span>
Dimanche = Sunday <audio controls> <source src = "fichieraudio/sunday.ogg"> </audio>
</span>
</p>

</article>
</section>

</body>

</html>
