<!DOCTYPE html>
<html lang = "fr">

<head>

<title>
Educafacile.com 
</title>

<link rel  ="stylesheet" href = "style.css"/>

<link rel  ="stylesheet" href = "menu.css"/>

<link rel  ="stylesheet" href = "text.css"/>

<meta name ="description" content = "Educafacile.com ressource libre"/>

<meta http-equiv="Content-Type" content="text/html;  charset=utf-8"/>

<meta name="author" content="massa anthony"/>

<?php 

include("section/piwik.php");

include("section/secure.php");

?>

</head>


<body>


<?php 

include("section/header.php");

include("section/nav.php");

?>

<section>

<article class = "center">
<h1 class = "blancstrong">
Cours  vocabulaire : la semaine
</h1>

<p class = "blancstrong">

le matin  <audio controls> <source src = "fichieraudio/morning.ogg"> </audio>
<br>
<br>

l'apr&egrave;s midi <audio controls><source src = "fichieraudio/evening.ogg"> </audio>
<br>
<br>
en fin d'apr&eacute;s midi&nbsp;&nbsp;<audio controls ><source src= "fichieraudio/afternoon.ogg"> </audio>
<br>
<br>
la nuit <audio controls> <source src = "fichieraudio/goodnight.ogg"> </audio>
<br>
<br>

</p>

</article>

</section>
<?php 

include("section/footer.php");

?>
</body>
</html>
